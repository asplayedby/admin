export const environment = {
  production: true,
  spotify_redirect: 'https://admin.asplayedby.com/login',
  spotify_client_id: '4d7ae8b469c34ff585168020c257f2b8',
  ingestion_url: 'https://api.asplayedby.com/set',
};
