// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  spotify_redirect: 'http://localhost:4200/login',
  spotify_client_id: '4d7ae8b469c34ff585168020c257f2b8',
  ingestion_url: 'https://api.asplayedby.com/set',
};
