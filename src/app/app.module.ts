import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { ArchwizardModule } from 'angular-archwizard';

import { AppComponent } from './app.component';
import { AddComponent } from './add/add.component';
import { SpotifyLoginComponent } from './spotify-login/spotify-login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import { SpotifyService } from './spotify.service';

import { environment } from '../environments/environment';
import { AddInitialFormComponent } from './add-initial-form/add-initial-form.component';
import { AddCorrectMatchesComponent } from './add-correct-matches/add-correct-matches.component';

const appRoutes: Routes = [
  { path: 'correct', component: AddCorrectMatchesComponent },
  { path: 'add', component: AddComponent },
  { path: 'login', component: SpotifyLoginComponent },
  { path: '', component: SpotifyLoginComponent },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    AddComponent,
    SpotifyLoginComponent,
    PageNotFoundComponent,
    AddInitialFormComponent,
    AddCorrectMatchesComponent,
  ],
  imports: [
    ArchwizardModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false }
    )
  ],
  providers: [SpotifyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
