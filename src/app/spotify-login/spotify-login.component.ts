import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { SpotifyService } from '../spotify.service';
import { Location } from '@angular/common';


@Component({
  selector: 'app-spotify-login',
  templateUrl: './spotify-login.component.html',
  styleUrls: ['./spotify-login.component.css']
})
export class SpotifyLoginComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private spotify: SpotifyService, private location: Location) { }

  private readonly state = 'asdf';
  public error: string;

  ngOnInit() {
    if (localStorage.getItem('spotify_code')) {
      // redirect to add
      this.router.navigate(['/add']);
    }

    this.route.queryParams.subscribe((queryParams) => {
      const state = queryParams.state;
      if (state && state !== this.state) {
        const message = `Callback called with invalid state parameter ${state}. Expected ${this.state}`;
        this.error = message;
        console.error(message);
        return;
      }
      const error = queryParams.error;
      if (error) {
        // do something
        this.error = error;
        console.error(error);
        return;
      }

      const code = queryParams.code;
      if (code) {
        localStorage.setItem('spotify_code', code);
        this.router.navigate(['/add']);
        return;
      }

      window.location.href = this.spotify.getAuthorizationUrl();

    });
  }

}
