export const response = {
    'set': {
        'playedAt': '2312-12-31T12:31:00.000Z',
        'playedBy': {
            'id': 1,
            'name': 'Pete Tong',
            'spotifyIdCandidates': ['6n1t55WMsSIUFHrAL4mUsB', '3WT64btTdTJmCY4gAGXvwf', '2dgaLLd4czspL28bdaBb9f'],
            'spotifyId': '6n1t55WMsSIUFHrAL4mUsB',
            'spotifyPlaylistId': null
        },
        'name': 'Essential Selection - 16th March 2018123',
        'spotifyPlaylistId': '032Bs7C6HzrQN6PLE6YLaq',
        'id': 8
    },
    'tracks': [{
        'id': 1,
        'name': 'Word Of Mouth  (feat. Bree Runway)',
        'spotifyId': null,
        'spotifyIdCandidates': []
    }, {
        'id': 26,
        'name': 'Wild Love (Shadow Child Remix)',
        'spotifyId': '2rXaCbITO0ACqoeFWqFBBK',
        'spotifyIdCandidates': ['2rXaCbITO0ACqoeFWqFBBK']
    }, {
        'id': 25,
        'name': 'Intergalactic Plastic',
        'spotifyId': '5xkDJ7Ju7ONXIIgvxZ5YNt',
        'spotifyIdCandidates': ['5xkDJ7Ju7ONXIIgvxZ5YNt', '7dYYezndRA5CoRTJIguzja', '04XC45vZhjEueHDRBFR9I5']
    }, {
        'id': 24,
        'name': 'Eastside (Ben Pearce Remix)  (feat. Soren Bryce)',
        'spotifyId': null,
        'spotifyIdCandidates': []
    }, {
        'id': 23,
        'name': 'Say We Will  (feat. Wolfgang Valbrun)',
        'spotifyId': null,
        'spotifyIdCandidates': []
    }, {
        'id': 22,
        'name': 'Roy Keane',
        'spotifyId': null,
        'spotifyIdCandidates': []
    }, {
        'id': 21,
        'name': 'Aura (Mele Remix)',
        'spotifyId': '2MI4HFRdd6gd9zc87YRjQk',
        'spotifyIdCandidates': ['2MI4HFRdd6gd9zc87YRjQk']
    }, {
        'id': 20,
        'name': 'Opal (Four Tet Remix)',
        'spotifyId': '3VtTuQ6lypMoOBcm6VMzdh',
        'spotifyIdCandidates': ['3VtTuQ6lypMoOBcm6VMzdh']
    }, {
        'id': 19,
        'name': 'Inhale  (feat. Ebenezer)',
        'spotifyId': null,
        'spotifyIdCandidates': []
    }, {
        'id': 18,
        'name': 'Britannia',
        'spotifyId': null,
        'spotifyIdCandidates': []
    }, {
        'id': 17,
        'name': 'Panic Room',
        'spotifyId': '7uUahoeqGNRMbpdeiLj1rS',
        'spotifyIdCandidates': ['7uUahoeqGNRMbpdeiLj1rS', '1wqCx4RZ8yABTkbh3Dx33W']
    }, {
        'id': 16,
        'name': 'Motorola',
        'spotifyId': '4bR87SmQTiNcS5gDuaF9Yl',
        'spotifyIdCandidates': ['4bR87SmQTiNcS5gDuaF9Yl']
    }, {
        'id': 15,
        'name': 'Ghost',
        'spotifyId': null,
        'spotifyIdCandidates': []
    }, {
        'id': 14,
        'name': 'Musika  (feat. Kwanzaa Posse)',
        'spotifyId': null,
        'spotifyIdCandidates': []
    }, {
        'id': 13,
        'name': 'The Return',
        'spotifyId': '6TwnXnCeolINrpjEJBtn2x',
        'spotifyIdCandidates': ['6TwnXnCeolINrpjEJBtn2x', '5LPTVPdLMkbJXkBhjHlAr1']
    }, {
        'id': 12,
        'name': 'Crowd Control',
        'spotifyId': '7ruylcpwEbeCl3HfB9SSyM',
        'spotifyIdCandidates': ['7ruylcpwEbeCl3HfB9SSyM']
    }, {
        'id': 11,
        'name': 'Ashes',
        'spotifyId': '0xRnJM4PNNWY7niasZImvM',
        'spotifyIdCandidates': ['0xRnJM4PNNWY7niasZImvM', '73St49IdOQyBTgSH9gfUiy']
    }, {
        'id': 10,
        'name': 'Stay Cool',
        'spotifyId': '6vlHJSDJjs2eN03mEVZJf6',
        'spotifyIdCandidates': ['6vlHJSDJjs2eN03mEVZJf6']
    }, {
        'id': 9,
        'name': 'Move On',
        'spotifyId': '2LrhfSwNRq7rg8SLPcgXja',
        'spotifyIdCandidates': ['2LrhfSwNRq7rg8SLPcgXja', '5kZrHar62qFY0AOiQDQ5Yh', '04PqMBFqqbT0Xql2LkgjCA',
            '4kNcafjkgAui6rW4afSKBy', '5FyKc7SeH03JdxzoEyhdN9', '43P9JFNgld1bBKmJDWeCgq', '2rArZEBDCQkEKDbGuBXT17',
            '0QR0v0H3Ev1w737FU1pkKe', '4NBtVo8emC1jQ30Gl2fIJz']
    }, {
        'id': 8,
        'name': 'I Found You (Black Coffee Journey Remix)',
        'spotifyId': '4Vmp3gvcOdCL0MUJxcdwyX',
        'spotifyIdCandidates': ['4Vmp3gvcOdCL0MUJxcdwyX', '7KV05eDSWT3BZC80XwyYrP', '3L9ryzKimLfJrDOQi7laNa', '7Ft95ekGYSSXjMmE6sBG2n']
    }, {
        'id': 7,
        'name': 'Roots',
        'spotifyId': '5fOpkVB2YYEZCApMjF5LgP',
        'spotifyIdCandidates': ['5fOpkVB2YYEZCApMjF5LgP', '50smGuLLyR3Q1sJhbor29w']
    }, {
        'id': 6,
        'name': 'Emerald Rush',
        'spotifyId': '2ZsJw2ppC0K381lFME21iX',
        'spotifyIdCandidates': ['2ZsJw2ppC0K381lFME21iX', '5Pw45Vda5KA0kHxxMR8wRM']
    }, {
        'id': 5,
        'name': 'Continuum',
        'spotifyId': null,
        'spotifyIdCandidates': []
    }, {
        'id': 4,
        'name': 'Infared',
        'spotifyId': null,
        'spotifyIdCandidates': []
    }, {
        'id': 3,
        'name': 'Reveal',
        'spotifyId': null,
        'spotifyIdCandidates': []
    }, {
        'id': 2,
        'name': 'Together',
        'spotifyId': null,
        'spotifyIdCandidates': []
    }]
};


