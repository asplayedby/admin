import { Component, OnInit, Input } from '@angular/core';

import { response } from './mock';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-add-correct-matches',
  templateUrl: './add-correct-matches.component.html',
  styleUrls: ['./add-correct-matches.component.css']
})
export class AddCorrectMatchesComponent implements OnInit {

  @Input()
  set: any;

  @Input()
  tracks: any;

  playlistEmbedUrl: SafeUrl;

  username: string = "burgerboy9n";

  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.set = response.set;
    this.tracks = response.tracks;
    const url = `https://open.spotify.com/embed?uri=spotify:user:${this.username}:playlist:${this.set.spotifyPlaylistId}&theme=black`;
    this.playlistEmbedUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  public trackEmbedUrl(track) {
    const url = `https://open.spotify.com/embed?uri=spotify%3Atrack%3A${track.spotifyId}`;
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

}
