import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParameterCodec } from '@angular/common/http';

import { environment } from '../../environments/environment';


@Component({
  selector: 'app-add-initial-form',
  templateUrl: './add-initial-form.component.html',
  styleUrls: ['./add-initial-form.component.css']
})
export class AddInitialFormComponent implements OnInit {

  private readonly ingestionUrl = environment.ingestion_url;

  model = new Set();
  result: any;
  error: string;

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  createSet() {
    console.log(this.model);
    const code = localStorage.getItem('spotify_code');
    const redirect = environment.spotify_redirect;
    this.http.post(this.ingestionUrl, this.model, { params: { code, redirect } }).subscribe(
      (result) => {
        console.log(result);
        this.result = result;
      },
      (err) => {
        console.error(err);
        this.error = JSON.stringify(err);
      },
    );
  }
}
