import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';


@Injectable()
export class SpotifyService {

  private readonly client_id = environment.spotify_client_id;
  private readonly redirect = environment.spotify_redirect;
  private readonly state = 'dunkeymonkey';
  private readonly scopes = ['playlist-read-private',
    'playlist-modify-private',
    'playlist-modify-public',
    'playlist-read-collaborative',
    'user-read-email',
    'user-read-private',
    'user-read-birthdate'];

  constructor() { }

  getAuthorizationUrl() {
    const url = 'https://accounts.spotify.com/authorize' +
      `?response_type=code` +
      `&client_id=${this.client_id}` +
      `&scope=${encodeURIComponent(this.scopes.join(' '))}` +
      '&redirect_uri=' + encodeURIComponent(this.redirect);
    return url;
  }

}
