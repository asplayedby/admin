import { Component, OnInit } from '@angular/core';
import { Set } from '../set';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  constructor(private router: Router) { }

  loggedIn = false;

  result: any;
  error: any;

  ngOnInit() {
    if (!localStorage.getItem('spotify_code')) {
      this.router.navigate(['/login']);
    }
    this.loggedIn = true;
  }

}
