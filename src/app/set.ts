export class Set {
    name: string;
    playedAt: Date;
    playedBy: string;
    setList: string;
}
